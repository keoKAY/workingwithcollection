import model.Person;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class MapDemo {
    public static void main(String[] args) {
        HashMap<String, Person>  map = new HashMap<>(){{
            put("james",new Person("james bond","male","usa"));
            put("bona",new Person("bona","male","cambodia"));
        }};

//        System.out.println(map.get("james"));
//        System.out.println(map.get("bona"));
//        System.out.println(map.get("nana"));
//        map.put(null , null );
        // to find what are the keys for our map
        for(String key : map.keySet()){
            System.out.println("key = " + key + " " + map.get(key));
        }
        for(Map.Entry<String,Person> entry : map.entrySet()){
            System.out.println(" ----------------------");
            System.out.println("Key : "+entry.getKey());
            System.out.println("Value : "+entry.getValue());
            System.out.println("--------------------------");
        }

    }
}
