import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class HashSetDemo {
    public static void main(String[] args) {
        ArrayList<String> names =
                new ArrayList<>(
                        List.of("James","James","June", "June","Bona","Bopha")
                );

        // to avoid complexity we use set to remove the duplicate
        HashSet<String> uniqueNames = new HashSet<String>(names);
        uniqueNames.add(null);
        uniqueNames.add(null);
//        for(String name : uniqueNames){
//            System.out.println("Name : "+name);
//        }
        System.out.println(uniqueNames);
//        names = new ArrayList<>(uniqueNames);
//        names.add("Vipha");
//        System.out.println("All name in the list are : ");
//        System.out.println(names);

    }
}
