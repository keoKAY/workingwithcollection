import model.Person;

import java.util.*;

public class TreeSetDemo {
    public static void main(String[] args) {
   /*     ArrayList<String> names =
                new ArrayList<>(
                        List.of("James","James","June", "June","Bona","Bopha")
                );
        // use treeSet in order to get rid of the duplicate , sort
        TreeSet<String> sortedUniqueNames = new TreeSet<>(names);
//        sortedUniqueNames.add(null);
        // ascending order
        System.out.println(sortedUniqueNames);
        // sort in descending order
        System.out.println(sortedUniqueNames.descendingSet());*/


        ArrayList<Person> personList = new ArrayList<>(){{
            for(int i = 1; i<=5; i++){
                add(new Person("username","male","cambodia"));
            }
            add(new Person("Jariya","female","cambodia"));
        }};

        HashSet<Person> uniquePerson = new HashSet<>(personList);
        personList = new ArrayList<>(uniquePerson);

        System.out.println("All Person ");
        for (Person person : personList){
            System.out.println(person);
        }





    }
}
