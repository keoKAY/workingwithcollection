import model.Person;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
            ArrayList<Person> personList = new ArrayList<>(){{
               // add(new Person("james","male","cambodia"));
                for(int i = 1 ; i <= 10; i++){
                    add(new Person("username"+i, "male","cambodia"));
                }
            }};



            // how many person in the list
        System.out.println(personList.size());
        Person updatedPerson = personList.get(9);
        updatedPerson.setName("James bond ");

        personList.set(9, updatedPerson);
        System.out.println("Successfully update the user at index 9 !! ");
        for(Person person : personList){
            System.out.println(person);
        }

        // find user when username10;
        System.out.println("Enter username you want to search : ");
        String username = input.nextLine();

        boolean isRecordFound = false;
        for(Person person : personList){
            if(person.getName().equals(username)){
                System.out.println(username + " exist.......!");
                System.out.println("This user is in the index of : "+personList.indexOf(person));
//                personList.remove(personList.indexOf(person));  // delete person by index
//                personList.remove(person);
                System.out.println(person);
                isRecordFound = true;
            }
        }
        if (!isRecordFound){
            System.out.println("There is no record with username = "+username);
        }


    }
}
